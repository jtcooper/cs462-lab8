ruleset manage_sensors {
  meta {
    shares __testing, sensors, sub_sensors, all_sensor_records
    provides sensors, sub_sensors, all_sensor_records, default_threshold, default_sms, sensor_tx_role
    use module io.picolabs.wrangler alias Wrangler
    use module io.picolabs.subscription alias Subscriptions
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" },
        // {"name": "sensors"},
        {"name": "sub_sensors"}
        // {"name": "all_sensor_records"}
      ] , "events": [
        {"domain": "sensor", "type": "new_sensor", "attrs": ["name", "profile_name"]},
        // {"domain": "sensor", "type": "unneeded_sensor", "attrs": ["Tx"]},
        {"domain": "sensor", "type": "unneeded_sensor", "attrs": ["name"]},
        {"domain": "sensor", "type": "delete_sensors"},
        {"domain": "sensor", "type": "subscribe_to_sensor", "attrs": ["name", "Tx", "Tx_host"]}
      ]
    }
    
    /** 
     * Constants 
     */
    default_threshold = function() {
      78
    }
    default_sms = function() {
      "+18018301745"
    }
    sensor_rx_role = "manager"
    sensor_rx_existing_role = "peer"
    sensor_tx_role = "sensor"
    
    // Returns a map of Tx eci's to sensor names (note: only includes child sensors created by this manager)
    sensors = function() {
      ent:sensors.defaultsTo({})
    }
    
    all_sensor_records = function() {
      // get all the temperature readings from each child sensor
      sub_sensors().map(function(value, key) {
        Wrangler:skyQuery(value{"Tx"}, "temperature_store", "temperatures")
      })
    }
    
    // Returns an array of subscriptions where the Tx_role is "sensor"
    sub_sensors = function() {
      Subscriptions:established().defaultsTo({})
          .filter(check_sub_is_sensor(sensor))
    }
    
    check_sub_is_sensor = function(sub) {
      // sensor{"Rx_role"} == sensor_rx_role && sensor{"Tx_role"} == sensor_tx_role
      sensor{"Tx_role"} == sensor_tx_role
    }
    
    get_tx_by_sensor_name = function(sensor_name) {
      sub_sensors()
          .filter(function(sensor) {ent:sensors{sensor{"Tx"}} == sensor_name})
          .head(){"Tx"}
    }
  }
  
  /** Rule creates a new sensor, if the name isn't already used */
  rule create_sensor {
    select when sensor new_sensor
    pre {
      sensor_name = event:attr("name")
      exists = ent:sensors.values() >< sensor_name
    }
    if not exists then
      noop()
    fired {
      raise wrangler event "child_creation"
        attributes { "name": sensor_name,
                     "color": "#00ffff",
                     "profile_name": event:attr("profile_name"),
                     "rids": ["temperature_store", "wovyn_base", 
                       "sensor_profile", "post_test", "io.picolabs.logging"]}
    }
  }
  
  /** 
   * Rule matches on creating a new sensor for an existing name, and displays an
   * error 
   */
  rule check_duplicate_sensor {
    select when sensor new_sensor
    pre {
      sensor_name = event:attr("name")
      exists = ent:sensors.values() >< sensor_name
    }
    if exists then
      send_directive("error", {"message": "sensor '" + sensor_name + "' already exists"})
  }
  
  /** 
   * Executes after the wrangler has created the child pico. This rule 
   * initializes the child's profile with appropriate values, and then 
   * initiates a subscription request.
   */
  rule new_sensor_initialized {
    select when wrangler child_initialized
    pre {
      sensor_name = event:attr("name")
      sensor_eci = event:attr("eci")
    }
    event:send({
      "eci": sensor_eci, 
      "eid": "child-initialization",
      "domain": "sensor", 
      "type": "profile_updated",
      "attrs": {
          "name": event:attr("rs_attrs"){"profile_name"}, 
          "threshold": default_threshold(), 
          "sms": default_sms()}
    })
    fired {
      // create a subscription to the child pico
      raise sensor event "subscribe_to_sensor"
        attributes {"name": sensor_name, "eci": sensor_eci};
    }
  }
  
  /** 
   * Executes after a child sensor has been successfully created and initialized
   * to create a communication tunnel with that sensor
   */
  rule subscribe_to_child_sensor {
    select when sensor subscribe_to_sensor where not event:attr("Tx")
    noop();
    fired {
      raise wrangler event "subscription"
        attributes {
          "wellKnown_Tx": event:attr("eci"),
          "name": event:attr("name"),
          "channel_type": "subscription",
          "Rx_role": sensor_rx_role,
          "Tx_role": sensor_tx_role
        };
    }
  }
  
  /**
   * Selects on the same "subscribe_to_sensor" domain type, but only when the
   * "Tx" attribute is present. When this occurs, we know that the sensor we're 
   * subscribing too isn't a child but an already existing sensor. This way,
   * the manager won't treat it as a deletable child pico.
   */
  rule subscribe_to_existing_sensor {
    select when sensor subscribe_to_sensor where event:attr("Tx")
    pre {
      host = (event:attr("Tx_host") == "" || not event:attr("Tx_host")) => null 
        | event:attr("Tx_host")
      params = {
        "wellKnown_Tx": event:attr("Tx"),
        "name": event:attr("name"),
        "channel_type": "subscription",
        "Rx_role": sensor_rx_existing_role, // Note that this value is different
        "Tx_role": sensor_tx_role,
        "Tx_host": host
      }
    }
    noop();
    fired {
      raise wrangler event "subscription"
        attributes params
    }
  }
  
  /**
   * Adds child sensors to entity variable. Only selects if the subscription 
   * roles match with the roles for a created child sensor.
   */
  rule new_sensor_entity {
    // for some reason the roles get flipped? I think this is because this event was raised by the child
    select when wrangler subscription_added where event:attr("Tx_role") == sensor_rx_role 
      && event:attr("Rx_role") == sensor_tx_role
    pre {
      b = event:attr("name").klog("Subscription success for:")
    }
    noop();
    fired {
      ent:sensors := ent:sensors.defaultsTo({});
      ent:sensors{[event:attr("Tx")]} := event:attr("name");
    }
  }
  
  /**
   * Executed when a new delete request comes in with a sensor Tx
   */
  rule delete_sensor_by_tx {
    select when sensor unneeded_sensor where event:attr("Tx")
    pre {
      tx = event:attr("Tx")
      exists = ent:sensors >< tx
    }
    if exists then noop()
    fired {
      raise wrangler event "child_deletion"
        attributes {"name": ent:sensors{tx}};
      raise wrangler event "subscription_cancellation"
        attributes {"Tx": tx};
      clear ent:sensors{[tx]};
    } else {
      send_directive("Could not find sensor with tx: " + tx);
    }
  }
  
  /**
   * Executed when a new delete request comes in with a sensor name
   */
  rule delete_sensor_by_name {
    select when sensor unneeded_sensor where event:attr("name")
    pre {
      sensor_name = event:attr("name")
      tx = get_tx_by_sensor_name(sensor_name).klog("tx:") // also guarantees it's a "child_sensor" role
    }
    if tx then
      send_directive("deleting sensor", {"sensor_name": sensor_name})
    fired {
      raise wrangler event "child_deletion"
        attributes {"name": sensor_name};
      raise wrangler event "subscription_cancellation"
        attributes {"Tx": tx};
      clear ent:sensors{[tx]};
    } else {
      send_directive("Could not find child sensor with name: " + sensor_name);
    }
  }
  
  rule clear_manager {
    select when sensor delete_sensors
      foreach ent:sensors setting (value, key)
    always {
      raise sensor event "unneeded_sensor"
        attributes {"name": key}
    }
  }
}

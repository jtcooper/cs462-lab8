ruleset wovyn_base {
  meta {
    shares __testing
    use module sensor_profile
    use module temperature_store
    use module io.picolabs.subscription alias Subscriptions
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      ] , "events": [
        {"domain": "wovyn", "type": "threshold_violation",
          "attrs": ["temperature"]}
      ]
    }
    
    sub_managers = function() {
      Subscriptions:established().defaultsTo({})
          .filter(check_sensor_is_sub(sub))
    }
    
    check_sensor_is_sub = function(sub) {
      sensor{"Rx_role"} == "sensor"
    }
  }
  
  rule process_heartbeat {
    select when wovyn heartbeat where event:attr("genericThing")
    pre {
      genericThing = event:attr("genericThing")
      temperature = genericThing{"data"}{"temperature"}
    }
    fired {
      raise wovyn event "new_temperature_reading"
        attributes {"temperature": temperature, "timestamp": time:now()}
    }
  }
  
  rule temperature_reading {
    select when wovyn new_temperature_reading
    pre {
      never_used = event:attrs.klog("attrs")
    }
  }
  
  rule find_high_temps {
    select when wovyn new_temperature_reading
    pre {
      temperature = event:attr("temperature")[0]{"temperatureF"}.klog("fahrenheit")
      temperatureThreshold = sensor_profile:threshold().klog("violation threshold:")
    }
    if temperature > temperatureThreshold then
      send_directive("temperature violation")
    fired {
      raise wovyn event "threshold_violation"
        attributes {"temperature": temperature, "timestamp": event:attr("timestamp")}
    }
  }
  
  rule threshold_notification {
    select when wovyn threshold_violation
    foreach sub_managers() setting(manager)
    event:send(
      { "eci": manager{"Tx"}, "eid": "sensor_reading",
        "domain": "sensor", "type": "threshold_violation",
        "attrs": { "temperature": event:attr("temperature"),
          "threshold": sensor_profile:threshold() }}
    )
  }
  
  /**
   * Auto-accept policy for subscription requests.
   */
  rule autoAccept {
    select when wrangler inbound_pending_subscription_added
    pre{
      attributes = event:attrs.klog("subcription :");
    }
    always{
      raise wrangler event "pending_subscription_approval"
          attributes attributes;       
      log info "auto accepted subcription.";
    }
  }
  
  /**
   * Listens for temperature report requests from manager Picos and returns the 
   * requested data.
   */
  rule return_temperature_report {
    select when sensor report
    pre {
      temperatures = temperature_store:temperatures()
      // locate the subscription being used
      sub = Subscriptions:established().defaultsTo({}).filter(
        function(sub) {
          sub{"Id"} == event:attr("subscription_id")
        })[0]  // index 0, since there can only be one subscription with this unique id
    }
    if sub then
      event:send({
        "eci": sub{"Tx"},
        "eid": event:attr("eid"),
        "domain": "report",
        "type": "catch_temperature_report",
        "attrs": {
          "subscription_id": event:attr("subscription_id"), 
          "eid": event:attr("eid"), 
          "temperatures": temperatures}
      })
    fired {
      send_directive("Submitted temperature report to subscription: " + event:attr("subscription_id"))
    } else {
      send_directive("Could not find subscription with id: " + event:attr("subscription_id"))
    }
    
  }
}

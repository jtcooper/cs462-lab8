ruleset post_test {
  meta {
    shares __testing
    use module temperature_store
  }
  global {
    __testing = {
      "queries": [ { "name": "__testing" } ],
      "events": [
        // {"domain": "post", "type": "test",
        //     "attrs": ["temp", "baro"]},
        {"domain": "wovyn_test", "type": "temperatures"},
        {"domain": "wovyn_test", "type": "violations"},
        {"domain": "wovyn_test", "type": "inrange"},
        {"domain": "wovyn_test", "type": "raise_violation",
            "attrs": ["temperature"]}
    ] }
  }

  rule post_test {
    select when post test
    pre {
      never_used = event:attrs.klog("attrs")
    }
  }

  rule get_temperatures {
    select when wovyn_test temperatures
    pre {
      temperatures = temperature_store:temperatures()
    }
    send_directive("response", {"temperatures": temperatures})
  }

  rule get_violations {
    select when wovyn_test violations
    pre {
      violations = temperature_store:threshold_violations()
    }
    send_directive("response", {"violations": violations})
  }

  rule get_inrange {
    select when wovyn_test inrange
    pre {
      inrange = temperature_store:inrange_temperatures()
    }
    send_directive("response", {"inrange_temperatures": inrange})
  }

  rule test_temperature_violation {
    select when wovyn_test raise_violation
    pre {
      temp_structure = [{"temperatureF": event:attr("temperature")}]
    }
    fired {
      raise wovyn event "new_temperature_reading"
        attributes {"temperature": temp_structure, "timestamp": time:now()}
    }
  }
}

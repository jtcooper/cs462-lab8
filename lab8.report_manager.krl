ruleset lab8.report_manager {
  meta {
    shares __testing, last_five_reports
    use module manage_sensors
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" },
        {"name": "last_five_reports"}
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [
        {"domain": "report", "type": "generate_report"},
        {"domain": "report", "type": "clear"}
      ]
    }
    
    last_five_reports = function() {
      // If we have 0 reports, this won't throw an error but just return an empty structure
      endIndex = (ent:report_order.length() >= 5) => 4 | ent:report_order.length() - 1;
      b = endIndex.klog("endIndex");
      ids = ent:report_order.reverse().slice(endIndex).klog("ids:");
      ids.map(function(id) {
        {}.put(id, ent:reports{id})
      });
    }
  }
  
  /**
   * Generate a new sensor report. First we initialize a data structure and 
   * some variables.
   */
  rule new_temperature_report {
    select when report generate_report
    pre {
      num_sensors = manage_sensors:sub_sensors().length()
      correlationId = random:uuid()
    }
    send_directive("Created new report: " + correlationId)
    always {
      ent:reports := ent:reports.defaultsTo({})
        .put(correlationId, {"temperature_sensors": num_sensors, 
          "responding": 0, "temperatures": {}});
      ent:report_order := ent:report_order.defaultsTo([]).append(correlationId);
      raise report event "send_report_requests"
        attributes {"correlationId": correlationId};
    }
  }
  
  rule propagate_report_requests {
    select when report send_report_requests
      foreach manage_sensors:sub_sensors() setting (sensor)
    pre {
      correlationId = event:attr("correlationId")
      b = klog(sensor)
    }
    if sensor{"Tx_role"} == manage_sensors:sensor_tx_role then
      event:send({
        "eci": sensor{"Tx"},
        "eid": correlationId,
        "domain": "sensor",
        "type": "report",
        "attrs": {
          "subscription_id": sensor{"Id"}, 
          "eid": correlationId}  // pass the eid in too, since I can't figure out how to access it through the event API
      })
  }
  
  rule catch_temperature_report {
    select when report catch_temperature_report
    pre {
      correlationId = event:attr("eid").klog("Caught report event for correlation id:")
      
      sensor = manage_sensors:sub_sensors().filter(function(sensor) {
        sensor{"Id"} == event:attr("subscription_id")
      }).klog("Matching sensor subscription")[0]
      
      oldReport = ent:reports{correlationId}.klog("old report:")
      
      // there's gotta be a better way to append to an internal data structure than this
      updatedTemperatures = oldReport{"temperatures"}
        .put(event:attr("subscription_id"), event:attr("temperatures")).klog("updated temperatures:")
        
      prevResponding = oldReport{"responding"}.klog("previous responding number:")
      
      reportS2 = oldReport.put("responding", prevResponding + 1).klog("report step 2:")
      
      updatedReport = reportS2.put("temperatures", updatedTemperatures).klog("updated report:")
      
    }
    if sensor then
      noop()
    fired {
      ent:reports{correlationId} := updatedReport;
    }
  }
  
  rule clear_reports {
    select when report clear
    always {
      ent:reports := {};
      ent:report_order := [];
    }
  }
}
